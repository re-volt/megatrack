# Megatrack

A project that strives to combine all stock tracks into one marathon track. More information in the RV Workshop.

# Workflow

Start with a track as a basis and add all the other tracks. Here **muse1** is used as base track.

Texture size is currently: *2048 x 2048*

1. Copy all instances to the muse1 folder
2. Combine textures - simply copy 256 x 256 texture from the track to an empty 256 x 256 field on the 2048 x 2048 texture in the muse1 folder.
3. Import only track (.w) using Marv's Addon in Blender - started with muse1
4. Select all objects, merge them, select all faces and switch to UV Editor. Scale everything by 0.125 and drag it over the right place. 
5. Split by loose parts again to later adjust everything
6. Import all instances, select every instance one by one and adjust the texture like for the track
7. Do the necassary adjustments in 3D. Connect the tracks, remove disturbing and double parts, ... Do not forget to move instances, when moving parts of the tracks.

# What's done so far (Changelog)

##### 2021-02-02
muse1 and muse2 are connected. Some modifications in the beginning of muse1 are made to make it fit in muse2. Also the part before the great hall are changed to fit. Instances were imported after the adjustments, so they had to be placed manually. Great reason to first import them, before changing anything in 3D. 3D connection is finished and working.


# What has to be done
Please see the issues section for that.

